package pl.edu.pw.okno.msi.neuralnet.activation;

public interface ActivationFunction {

  double getValue(double input);

  double getDerivative(double input);
}
