package pl.edu.pw.okno.msi.neuralnet.neuron;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TrainingRate {

  @Getter private final double rate;
}
