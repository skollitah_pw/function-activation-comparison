package pl.edu.pw.okno.msi;

import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.argument.GenerateExamplesCommand;
import pl.edu.pw.okno.msi.argument.GenerateWeightsCommand;
import pl.edu.pw.okno.msi.argument.RunCommand;
import pl.edu.pw.okno.msi.argument.TrainCommand;
import pl.edu.pw.okno.msi.process.CommandProcessor;

@Slf4j
public class Application {

  public static void main(String[] args) {

    TrainCommand trainCommand = new TrainCommand();
    RunCommand runCommand = new RunCommand();
    GenerateWeightsCommand generateWeightsCommand = new GenerateWeightsCommand();
    GenerateExamplesCommand generateExamplesCommand = new GenerateExamplesCommand();

    JCommander jc =
        JCommander.newBuilder()
            .addCommand(trainCommand)
            .addCommand(runCommand)
            .addCommand(generateWeightsCommand)
            .addCommand(generateExamplesCommand)
            .build();

    jc.parse(args);

    CommandProcessor processManager = new CommandProcessor();

    switch (jc.getParsedCommand()) {
      case TrainCommand.NAME:
        processManager.train(trainCommand);
        break;
      case RunCommand.NAME:
        processManager.run(runCommand);
        break;
      case GenerateWeightsCommand.NAME:
        processManager.generateWeights(generateWeightsCommand);
        break;
      case GenerateExamplesCommand.NAME:
        processManager.generateExamples(generateExamplesCommand);
        break;
      default:
        throw new IllegalArgumentException("Unknown command");
    }
  }
}
