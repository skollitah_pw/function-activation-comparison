package pl.edu.pw.okno.msi.function;

import java.util.List;
import lombok.ToString;
import pl.edu.pw.okno.msi.utils.Preconditions;

@ToString
public class SevenArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);
    double z = arguments.get(2);
    double v = arguments.get(3);
    double w = arguments.get(4);
    double m = arguments.get(5);
    double n = arguments.get(6);

    return -1.0 * Math.pow(x, 3.0)
        - 5.0 * Math.pow(y, 2.0)
        - 6.0 * Math.pow(z, 5.0)
        + 4.0 * Math.pow(v, -2.0)
        + 1.0 * Math.pow(v, -1.0)
        + 4.0 * Math.pow(m, 2.0)
        - 8.0 * n
        + 9.0;
  }

  @Override
  public int getNumberOfArguments() {
    return 7;
  }
}
