package pl.edu.pw.okno.msi.argument;

import static pl.edu.pw.okno.msi.argument.GenerateWeightsCommand.NAME;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Parameters(commandNames = NAME, commandDescription = "Generate initial weights")
@Getter
@Setter
@ToString
public class GenerateWeightsCommand {

  public static final String NAME = "generate-weights";

  @Parameter(names = "-i", description = "Number of network inputs", required = true)
  private Integer numberOfInputs;

  @Parameter(names = "-o", description = "Number of network outputs", required = true)
  private Integer numberOfOutputs;

  @Parameter(
      names = "-h",
      description = "Number of hidden neurons separated by comma: 6,8",
      required = true)
  private List<Integer> hiddenLayersSizes;

  @Parameter(names = "-r", description = "Weights range around 0", required = true)
  private Double weightInitRange;

  @Parameter(names = "--weights", description = "Path to generated weights", required = true)
  private String pathToGeneratedWeights;
}
