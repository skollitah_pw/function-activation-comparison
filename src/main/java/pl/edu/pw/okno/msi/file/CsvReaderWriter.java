package pl.edu.pw.okno.msi.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CsvReaderWriter {

  public List<List<Double>> read(Path path) {
    try (FileReader fr = new FileReader(path.toFile());
        BufferedReader bf = new BufferedReader(fr)) {

      return bf.lines()
          .map(
              line ->
                  Arrays.stream(line.split(";"))
                      .map(Double::parseDouble)
                      .collect(Collectors.toList()))
          .collect(Collectors.toList());

    } catch (IOException e) {
      throw new IllegalArgumentException(String.format("Error reading file %s", path.toString()));
    }
  }

  public void write(Path path, List<List<Double>> records) {
    try (FileWriter fw = new FileWriter(path.toFile());
        BufferedWriter bw = new BufferedWriter(fw)) {

      List<String> csvRecords =
          records.stream()
              .map(
                  record -> record.stream().map(Objects::toString).collect(Collectors.joining(";")))
              .collect(Collectors.toList());

      for (String csvRecord : csvRecords) {
        bw.write(csvRecord);
        bw.newLine();
      }

    } catch (IOException e) {
      throw new IllegalArgumentException(String.format("Error reading file %s", path.toString()));
    }
  }

  public Path getFilePathFromClassPath(String name) {
    try {
      return Paths.get(CsvReaderWriter.class.getClassLoader().getResource(name).toURI());
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException(String.format("Error reading file %s", name));
    }
  }
}
