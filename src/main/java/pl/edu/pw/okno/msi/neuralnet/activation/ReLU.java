package pl.edu.pw.okno.msi.neuralnet.activation;

import lombok.ToString;

@ToString
public class ReLU implements ActivationFunction {

  @Override
  public double getValue(double input) {
    return Math.max(0.0, input);
  }

  @Override
  public double getDerivative(double input) {
    return input <= 0.0 ? 0.0 : 1.0;
  }
}
