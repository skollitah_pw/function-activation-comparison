package pl.edu.pw.okno.msi.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import pl.edu.pw.okno.msi.function.MArgumentFunction;
import pl.edu.pw.okno.msi.utils.Pair;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class ExamplesGenerator {

  public Pair<List<List<Double>>, List<List<Double>>> generateExamples(
      List<Pair<Double, Double>> argumentRanges,
      List<MArgumentFunction> mArgumentFunctions,
      int numberOfExamples) {

    Preconditions.checkArgument(argumentRanges != null, "argumentRanges is null");
    Preconditions.checkArgument(mArgumentFunctions != null, "mArgumentFunctions is null");
    Preconditions.checkArgument(numberOfExamples > 0, "numberOfExamples is less or equal 0");
    Preconditions.checkArgument(
        argumentRanges.size()
            == mArgumentFunctions.stream()
                .mapToInt(MArgumentFunction::getNumberOfArguments)
                .max()
                .orElse(0),
        "mArgumentFunctions parameters size different than argumentRanges size");
    Preconditions.checkArgument(
        argumentRanges.stream()
            .allMatch(argumentRange -> argumentRange.getValue0() < argumentRange.getValue1()),
        "range from less or equal range to");

    List<List<Double>> inputValues = new ArrayList<>();
    List<List<Double>> outputValues = new ArrayList<>();

    IntStream.range(0, numberOfExamples)
        .forEach(
            exampleIndex -> {
              Pair<List<Double>, List<Double>> trainRecord =
                  generateTrainRecord(argumentRanges, mArgumentFunctions);
              inputValues.add(trainRecord.getValue0());
              outputValues.add(trainRecord.getValue1());
            });

    return Pair.with(inputValues, outputValues);
  }

  private Pair<List<Double>, List<Double>> generateTrainRecord(
      List<Pair<Double, Double>> argumentRanges, List<MArgumentFunction> mArgumentFunctions) {

    List<Double> inputValuesRecord = new ArrayList<>();

    argumentRanges.forEach(
        argumentRange ->
            inputValuesRecord.add(
                (Math.random()) * (argumentRange.getValue1() - argumentRange.getValue0())
                    + argumentRange.getValue0()));

    List<Double> outputsRecord =
        mArgumentFunctions.stream()
            .map(
                mArgumentFunction ->
                    mArgumentFunction.getValue(
                        inputValuesRecord.stream()
                            .limit(mArgumentFunction.getNumberOfArguments())
                            .collect(Collectors.toList())))
            .collect(Collectors.toList());

    return Pair.with(inputValuesRecord, outputsRecord);
  }

  public <T> Pair<List<List<T>>, List<List<T>>> splitInputAndOutput(
      int numberOfInputs, int numberOffOutputs, List<List<T>> combinedInputsAndOutputs) {

    Preconditions.checkArgument(numberOfInputs > 0, "numberOfInputs less or equal 0");
    Preconditions.checkArgument(numberOffOutputs > 0, "numberOffOutputs less or equal 0");
    Preconditions.checkArgument(
        combinedInputsAndOutputs != null, "combinedInputsAndOutputs is null");
    Preconditions.checkArgument(
        combinedInputsAndOutputs.stream()
            .allMatch(record -> record.size() == numberOfInputs + numberOffOutputs),
        "record size different than sum of inputs and outputs");

    List<List<T>> inputValuesRecord =
        combinedInputsAndOutputs.stream()
            .map(combinedRecord -> combinedRecord.subList(0, numberOfInputs))
            .collect(Collectors.toList());

    List<List<T>> outputsRecord =
        combinedInputsAndOutputs.stream()
            .map(combinedRecord -> combinedRecord.subList(numberOfInputs, combinedRecord.size()))
            .collect(Collectors.toList());

    return Pair.with(inputValuesRecord, outputsRecord);
  }

  public <T> List<List<T>> combineInputAndOutput(
      Pair<List<List<T>>, List<List<T>>> pairOfInputsAndOutputs) {

    Preconditions.checkArgument(pairOfInputsAndOutputs != null, "pairOfInputsAndOutputs is null");
    Preconditions.checkArgument(
        pairOfInputsAndOutputs.getValue0().size() == pairOfInputsAndOutputs.getValue1().size(),
        "number of inputs is different that number of outputs");
    Preconditions.checkArgument(
        pairOfInputsAndOutputs.getValue0().stream().map(List::size).distinct().count() == 1,
        "input records have different size");
    Preconditions.checkArgument(
        pairOfInputsAndOutputs.getValue1().stream().map(List::size).distinct().count() == 1,
        "output records have different size");

    return IntStream.range(0, pairOfInputsAndOutputs.getValue0().size())
        .mapToObj(
            index -> {
              List<T> combined = new ArrayList<>(pairOfInputsAndOutputs.getValue0().get(index));
              combined.addAll(pairOfInputsAndOutputs.getValue1().get(index));
              return combined;
            })
        .collect(Collectors.toList());
  }

  public List<Double> generateWeights(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      Double weightInitRange) {

    checkNetParametersArguments(numberOfInputs, numberOfOutputs, hiddenLayersSizes);
    Preconditions.checkArgument(
        weightInitRange != null && weightInitRange > 0,
        "weightInitRange is null or less or equal 0");

    return IntStream.range(
            0,
            getNumberOfWeightsForNetParameters(numberOfInputs, numberOfOutputs, hiddenLayersSizes))
        .mapToObj(index -> (Math.random() - 0.5) * weightInitRange)
        .collect(Collectors.toList());
  }

  public int getNumberOfWeightsForNetParameters(
      int numberOfInputs, int numberOfOutputs, List<Integer> hiddenLayersSizes) {

    checkNetParametersArguments(numberOfInputs, numberOfOutputs, hiddenLayersSizes);

    // Weights between input layer and first hidden layer
    int numberOfWeights = numberOfInputs * hiddenLayersSizes.get(0);

    // Weights between last hidden layer and output layer
    numberOfWeights += hiddenLayersSizes.get(hiddenLayersSizes.size() - 1) * numberOfOutputs;

    // Weights between biases and hidden layers
    numberOfWeights += hiddenLayersSizes.stream().mapToInt(Integer::intValue).sum();

    // Weights between last hidden layer bias and output layer
    numberOfWeights += numberOfOutputs;

    // Weights between hidden layers
    numberOfWeights +=
        IntStream.range(0, hiddenLayersSizes.size() - 1)
            .map(index -> hiddenLayersSizes.get(index) * hiddenLayersSizes.get(index + 1))
            .sum();

    return numberOfWeights;
  }

  private void checkNetParametersArguments(
      int numberOfInputs, int numberOfOutputs, List<Integer> hiddenLayersSizes) {
    Preconditions.checkArgument(numberOfInputs > 0, "numberOfInputs is less or equal 0");
    Preconditions.checkArgument(numberOfOutputs > 0, "numberOfInputs is less or equal 0");
    Preconditions.checkArgument(
        hiddenLayersSizes != null
            && hiddenLayersSizes.stream().allMatch(hiddenLayersSize -> hiddenLayersSize > 0),
        "hiddenLayersSizes is null or sizes less or equal 0");
  }
}
