package pl.edu.pw.okno.msi.neuralnet.neuron;

public interface ValueProvider {

  double getValue();
}
