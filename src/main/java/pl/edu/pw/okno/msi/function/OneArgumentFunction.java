package pl.edu.pw.okno.msi.function;

import java.util.List;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class OneArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);

    return 3.0 * Math.pow(x, 2.0) + 10.0;
  }

  @Override
  public int getNumberOfArguments() {
    return 1;
  }
}
