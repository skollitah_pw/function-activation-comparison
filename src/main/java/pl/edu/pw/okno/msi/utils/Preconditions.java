package pl.edu.pw.okno.msi.utils;

public class Preconditions {

  public static void checkArgument(boolean condition, String message) {
    if (!condition) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkState(boolean condition, String message) {
    if (!condition) {
      throw new IllegalStateException(message);
    }
  }

  public static <T> void checkNotNull(T object, String message) {
    if (object == null) {
      throw new NullPointerException(message);
    }
  }
}
