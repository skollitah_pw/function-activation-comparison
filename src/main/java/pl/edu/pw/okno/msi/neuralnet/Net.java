package pl.edu.pw.okno.msi.neuralnet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.neuralnet.neuron.Bias;
import pl.edu.pw.okno.msi.neuralnet.neuron.Hidden;
import pl.edu.pw.okno.msi.neuralnet.neuron.Input;
import pl.edu.pw.okno.msi.neuralnet.neuron.Neuron;
import pl.edu.pw.okno.msi.neuralnet.neuron.Output;
import pl.edu.pw.okno.msi.neuralnet.neuron.WeightedLink;
import pl.edu.pw.okno.msi.utils.Preconditions;

@Slf4j
@RequiredArgsConstructor
public class Net {

  private final List<Input> inputs;
  private final List<HiddenLayer> hiddenLayers;
  private final List<Bias> hiddenBiases;
  private final List<Output> outputs;
  @Getter private final Bias outputBias;
  private final List<WeightedLink> weightedLinks;

  public void backPropagateError(
      List<Double> inputValues, List<Double> expectedOutputs, double trainingRate) {
    Preconditions.checkNotNull(expectedOutputs, "expectedOutputs is null");
    Preconditions.checkArgument(
        expectedOutputs.size() == outputs.size(),
        "expectedOutputs has different size than outputs");
    Preconditions.checkNotNull(inputValues, "inputs is null");
    Preconditions.checkArgument(
        inputValues.size() == inputs.size(), "inputValues has different size than inputs");
    Preconditions.checkArgument(trainingRate > 0, "trainingRate is less or equal 0");

    Map<String, Double> inputsCache = new ConcurrentHashMap<>();
    getAllNeurons().forEach(neuron -> neuron.setInputsCache(inputsCache));
    setInputValues(inputValues);
    setExpectedOutputs(expectedOutputs);
    weightedLinks.parallelStream().forEach(WeightedLink::calculateGradient);
    weightedLinks.forEach(weightedLink -> weightedLink.updateWeight(trainingRate));
  }

  public double totalError(List<Double> inputValues, List<Double> expectedOutputs) {
    setInputValues(inputValues);
    setExpectedOutputs(expectedOutputs);
    Map<String, Double> inputsCache = new ConcurrentHashMap<>();
    getAllNeurons().forEach(neuron -> neuron.setInputsCache(inputsCache));
    return outputs.parallelStream().mapToDouble(Output::getError).sum();
  }

  public void train(
      int maxIterations,
      double expectedError,
      double trainingRate,
      List<List<Double>> inputValues,
      List<List<Double>> expectedOutputs) {

    Preconditions.checkArgument(maxIterations > 0, "maxIterations <= 0");
    Preconditions.checkArgument(expectedError > 0.0, "expectedError <= 0.0");
    Preconditions.checkArgument(
        inputValues.stream()
            .allMatch(inputValuesRecord -> inputValuesRecord.size() == inputs.size()),
        "inputValues size not equal to network inputs size");
    Preconditions.checkArgument(
        expectedOutputs.stream()
            .allMatch(expectedOutputsRecord -> expectedOutputsRecord.size() == outputs.size()),
        "expectedOutputs size not equal to network outputs size");
    Preconditions.checkArgument(
        inputValues.size() == expectedOutputs.size(),
        "inputValues size not equal to expectedOutputs size");
    Preconditions.checkArgument(trainingRate > 0, "trainingRate is less or equal 0");

    for (int iteration = 0; iteration < maxIterations; iteration++) {

      IntStream.range(0, inputValues.size())
          .forEach(
              trainSetIndex ->
                  backPropagateError(
                      inputValues.get(trainSetIndex),
                      expectedOutputs.get(trainSetIndex),
                      trainingRate));

      double totalMeanError = totalMeanError(inputValues, expectedOutputs);

      log.info(String.format("Iteration: [%s] Total error: [%s]", iteration, totalMeanError));

      if (totalMeanError < expectedError) {
        break;
      }
    }
  }

  private List<Neuron> getAllNeurons() {
    List<Neuron> neurons = new ArrayList<>(outputs);
    neurons.addAll(
        hiddenLayers.stream()
            .map(HiddenLayer::getNeurons)
            .flatMap(List::stream)
            .collect(Collectors.toList()));
    return neurons;
  }

  // TODO what exactly I computes here
  private double totalMeanError(
      List<List<Double>> inputValues, List<List<Double>> expectedOutputs) {
    return IntStream.range(0, inputValues.size())
        .mapToDouble(
            trainSetIndex ->
                totalError(inputValues.get(trainSetIndex), expectedOutputs.get(trainSetIndex)))
        .average()
        .orElse(0.0);
  }

  public List<Double> totalMeanErrorForOutputs(
      List<List<Double>> inputValues, List<List<Double>> expectedOutputs) {

    Preconditions.checkArgument(
        inputValues.stream()
            .allMatch(inputValuesRecord -> inputValuesRecord.size() == inputs.size()),
        "inputValues size not equal to network inputs size");
    Preconditions.checkArgument(
        expectedOutputs.stream()
            .allMatch(expectedOutputsRecord -> expectedOutputsRecord.size() == outputs.size()),
        "expectedOutputs size not equal to network outputs size");
    Preconditions.checkArgument(
        inputValues.size() == expectedOutputs.size(),
        "inputValues size not equal to expectedOutputs size");

    List<List<Double>> errorsForOutputsForAllExamples =
        IntStream.range(0, inputValues.size())
            .mapToObj(
                trainSetIndex ->
                    errorForOutputs(
                        inputValues.get(trainSetIndex), expectedOutputs.get(trainSetIndex)))
            .collect(Collectors.toList());

    return IntStream.range(0, outputs.size())
        .mapToObj(
            outputIndex ->
                errorsForOutputsForAllExamples.stream()
                    .mapToDouble(e -> e.get(outputIndex))
                    .average()
                    .orElse(0.0))
        .collect(Collectors.toList());
  }

  private List<Double> errorForOutputs(List<Double> inputValues, List<Double> expectedOutputs) {
    setInputValues(inputValues);
    setExpectedOutputs(expectedOutputs);
    Map<String, Double> inputsCache = new ConcurrentHashMap<>();
    getAllNeurons().forEach(neuron -> neuron.setInputsCache(inputsCache));
    return outputs.parallelStream().map(Output::getError).collect(Collectors.toList());
  }

  private void setExpectedOutputs(List<Double> expectedOutputs) {
    IntStream.range(0, outputs.size())
        .forEach(index -> outputs.get(index).setExpectedOutput(expectedOutputs.get(index)));
  }

  public List<Double> forwardPropagate(List<Double> inputValues) {
    Preconditions.checkNotNull(inputValues, "inputs is null");
    Preconditions.checkArgument(
        inputValues.size() == inputs.size(), "inputValues has different size than inputs");

    Map<String, Double> inputsCache = new ConcurrentHashMap<>();
    getAllNeurons().forEach(neuron -> neuron.setInputsCache(inputsCache));
    setInputValues(inputValues);

    return getOutputValues();
  }

  private void setInputValues(List<Double> inputValues) {
    IntStream.range(0, inputs.size())
        .forEach(index -> inputs.get(index).setValue(inputValues.get(index)));
  }

  private List<Double> getOutputValues() {
    return outputs.stream().map(Output::getValue).collect(Collectors.toList());
  }

  public List<Double> getWeights() {
    return weightedLinks.stream().map(WeightedLink::getWeight).collect(Collectors.toList());
  }

  public Input findInput(int index) {
    return inputs.stream()
        .filter(input -> input.getIndex() == index)
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Input with index: %s not found", index)));
  }

  public Bias findHiddenBias(int layerIndex) {
    return hiddenBiases.stream()
        .filter(
            bias ->
                bias.getLayerIndex() == layerIndex
                    && bias.getIndex() == hiddenLayers.get(layerIndex).getNeurons().size())
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Bias with layerIndex: %s not found", layerIndex)));
  }

  public Output findOutput(int index) {
    return outputs.stream()
        .filter(output -> output.getIndex() == index)
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Output with index: %s not found", index)));
  }

  public Hidden findHidden(int layerIndex, int index) {
    return hiddenLayers.stream()
        .map(Net.HiddenLayer::getNeurons)
        .flatMap(List::stream)
        .filter(hidden -> hidden.getLayerIndex() == layerIndex && hidden.getIndex() == index)
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format(
                        "Hidden with layerIndex: %s, index: %s not found", layerIndex, index)));
  }

  public WeightedLink findLink(
      int inputLayerIndex, int inputIndex, int outputLayerIndex, int outputIndex) {

    return weightedLinks.stream()
        .filter(
            weightedLink ->
                weightedLink.getInputLayerIndex() == inputLayerIndex
                    && weightedLink.getInputIndex() == inputIndex
                    && weightedLink.getOutputLayerIndex() == outputLayerIndex
                    && weightedLink.getOutputIndex() == outputIndex)
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format(
                        "WeightedLink with inputLayerIndex: %s, inputIndex: %s, outputLayerIndex: %s, outputIndex: %s not found",
                        inputLayerIndex, inputIndex, outputLayerIndex, outputIndex)));
  }

  @RequiredArgsConstructor
  static class HiddenLayer {

    @Getter private final List<Hidden> neurons;
  }
}
