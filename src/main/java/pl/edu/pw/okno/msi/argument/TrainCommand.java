package pl.edu.pw.okno.msi.argument;

import static pl.edu.pw.okno.msi.argument.TrainCommand.NAME;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;

@Parameters(commandNames = NAME, commandDescription = "Train neural network")
@Getter
@Setter
@ToString
public class TrainCommand {

  public static final String NAME = "train";

  @Parameter(names = "-i", description = "Number of network inputs", required = true)
  private Integer numberOfInputs;

  @Parameter(names = "-o", description = "Number of network outputs", required = true)
  private Integer numberOfOutputs;

  @Parameter(
      names = "-h",
      description = "Number of hidden neurons separated by comma: 6,8",
      required = true)
  private List<Integer> hiddenLayersSizes;

  @Parameter(names = "-t", description = "Number of iterations")
  private Integer numOfIterations;

  @Parameter(names = "-e", description = "Expected maximum network total error")
  private Double expectedTotalError;

  @Parameter(names = "-r", description = "Initial weights range")
  private Double initialWeightsRange;

  @Parameter(names = "-a", description = "Training rate", required = true)
  private Double trainingRate;

  @Parameter(
      names = "--fh",
      description =
          "Activation function for hidden layers. One of: R - (ReLu), L - (Linear), S - (Sigmoid)",
      converter = ActivationFunctionConverter.class,
      required = true)
  private ActivationFunction hiddenLayerActivationFunction;

  @Parameter(
      names = "--fo",
      description = "Activation function for output layer",
      converter = ActivationFunctionConverter.class,
      required = true)
  private ActivationFunction outputLayerActivationFunction;

  @Parameter(names = "--examples", description = "Path to training examples", required = true)
  private String pathToTrainingExamples;

  @Parameter(names = "--initial-weights", description = "Path to initial weights")
  private String pathToInitialWeights;

  @Parameter(names = "--output-weights", description = "Path to output weights")
  private String pathToOutputWeights;
}
