package pl.edu.pw.okno.msi.neuralnet.neuron;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public class Bias implements ValueProvider {

  @Getter private final int layerIndex;
  @Getter private final int index;

  @Override
  public double getValue() {
    return 1.0;
  }
}
