package pl.edu.pw.okno.msi.process;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.argument.GenerateWeightsCommand;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;

@Slf4j
@RequiredArgsConstructor
public class GenerateWeightsCommandProcessor {

  private final GenerateWeightsCommand generateWeightsCommand;
  private final ExamplesGenerator examplesGenerator;
  private final CsvReaderWriter csvReaderWriter;

  public void process() {
    log.info("Generate weights for command: " + generateWeightsCommand);

    List<Double> generatedWeights =
        examplesGenerator.generateWeights(
            generateWeightsCommand.getNumberOfInputs(),
            generateWeightsCommand.getNumberOfOutputs(),
            generateWeightsCommand.getHiddenLayersSizes(),
            generateWeightsCommand.getWeightInitRange());

    Path outputPath = Paths.get(generateWeightsCommand.getPathToGeneratedWeights());

    csvReaderWriter.write(outputPath, Collections.singletonList(generatedWeights));

    log.info(String.format("Weights generated to: %s", outputPath.toAbsolutePath().toString()));
  }
}
