package pl.edu.pw.okno.msi.neuralnet.neuron;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Output extends Neuron {

  @Getter private final int layerIndex;
  @Getter private final int index;
  @Getter @Setter private double expectedOutput;

  public Output(int layerIndex, int index) {
    this.layerIndex = layerIndex;
    this.index = index;
    computeIdentifier();
  }

  public double getErrorDerivative() {
    return -(expectedOutput - getValue());
  }

  public double getError() {
    return Math.pow(expectedOutput - getValue(), 2);
  }
}
