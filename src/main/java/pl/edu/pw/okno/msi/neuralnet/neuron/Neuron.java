package pl.edu.pw.okno.msi.neuralnet.neuron;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;
import pl.edu.pw.okno.msi.utils.Preconditions;

@Slf4j
public abstract class Neuron implements ValueProvider {

  @Getter @Setter protected ActivationFunction activationFunction;
  @Getter @Setter protected List<WeightedLink> inputLinks;
  private String identifier;

  @Getter @Setter private Map<String, Double> inputsCache;

  public double getValue() {
    Preconditions.checkState(inputsCache != null, "inputsCache is null");

    Double memoizedValue = inputsCache.get(identifier);

    double value;

    if (memoizedValue != null) {
      value = memoizedValue;
    } else {
      value = activationFunction.getValue(sumWightedInputs());
      inputsCache.put(identifier, value);
    }

    return value;
  }

  private double sumWightedInputs() {
    return inputLinks.stream()
        .mapToDouble(inputLink -> inputLink.getWeight() * inputLink.getInput().getValue())
        .sum();
  }

  public double getDerivative() {
    return activationFunction.getDerivative(sumWightedInputs());
  }

  public void addInputLink(WeightedLink inputLink) {
    if (inputLinks == null) {
      inputLinks = new ArrayList<>();
    }

    inputLinks.add(inputLink);
  }

  protected abstract int getIndex();

  protected abstract int getLayerIndex();

  public void computeIdentifier() {
    identifier = "" + getLayerIndex() + getIndex();
  }

  public abstract double getErrorDerivative();
}
