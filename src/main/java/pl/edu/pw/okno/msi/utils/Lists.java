package pl.edu.pw.okno.msi.utils;

import java.util.ArrayList;
import java.util.List;

public class Lists {

  public static <T> List<T> concat(List<T> first, List<T> second) {
    List<T> result = new ArrayList<>(first);
    result.addAll(second);
    return result;
  }
}
