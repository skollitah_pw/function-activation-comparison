package pl.edu.pw.okno.msi.neuralnet.neuron;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pw.okno.msi.utils.Preconditions;

@ToString(exclude = {"input", "output"})
public class WeightedLink {

  @Getter private final int inputLayerIndex;
  @Getter private final int inputIndex;
  @Getter private final int outputLayerIndex;
  @Getter private final int outputIndex;

  @Getter @Setter private ValueProvider input;

  @Getter @Setter private Neuron output;

  @Getter @Setter private double weight;

  private Double gradient = null;

  public WeightedLink(int inputLayerIndex, int inputIndex, int outputLayerIndex, int outputIndex) {
    this.inputLayerIndex = inputLayerIndex;
    this.inputIndex = inputIndex;
    this.outputLayerIndex = outputLayerIndex;
    this.outputIndex = outputIndex;
  }

  public void calculateGradient() {
    gradient = calculateDelta() * input.getValue();
  }

  private double calculateDelta() {
    return output.getErrorDerivative() * output.getDerivative();
  }

  public void updateWeight(double trainingRate) {
    Preconditions.checkArgument(trainingRate > 0, "trainingRate is less or equal 0");
    Preconditions.checkState(gradient != null, "gradient is null");

    weight = weight - trainingRate * gradient;
    gradient = null;
  }
}
