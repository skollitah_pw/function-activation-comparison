package pl.edu.pw.okno.msi.argument;

import static pl.edu.pw.okno.msi.argument.RunCommand.NAME;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;

@Parameters(commandNames = NAME, commandDescription = "Run neural network for given input")
@Getter
@Setter
@ToString
public class RunCommand {

  public static final String NAME = "run";

  @Parameter(names = "-i", description = "Number of network inputs", required = true)
  private Integer numberOfInputs;

  @Parameter(names = "-o", description = "Number of network outputs", required = true)
  private Integer numberOfOutputs;

  @Parameter(
      names = "-h",
      description = "Number of hidden neurons separated by comma: 6,8",
      required = true)
  private List<Integer> hiddenLayersSizes;

  @Parameter(
      names = "--fh",
      description =
          "Activation function for hidden layers. One of: R - (ReLu), L - (Linear), S - (Sigmoid)",
      converter = ActivationFunctionConverter.class,
      required = true)
  private ActivationFunction hiddenLayerActivationFunction;

  @Parameter(
      names = "--fo",
      description = "Activation function for output layer",
      converter = ActivationFunctionConverter.class,
      required = true)
  private ActivationFunction outputLayerActivationFunction;

  @Parameter(names = "--inputs", description = "Path to inputs", required = true)
  private String pathToInputs;

  @Parameter(names = "--weights", description = "Path to initial weights", required = true)
  private String pathToWeights;
}
