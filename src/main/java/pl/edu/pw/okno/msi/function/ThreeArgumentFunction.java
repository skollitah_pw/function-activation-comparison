package pl.edu.pw.okno.msi.function;

import java.util.List;
import lombok.ToString;
import pl.edu.pw.okno.msi.utils.Preconditions;

@ToString
public class ThreeArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);
    double z = arguments.get(2);

    return 3.0 * Math.pow(x, 4.0) - 5.0 * Math.pow(y, -2.0) + 2.0 * Math.pow(z, 2.0) + 4.0;
  }

  @Override
  public int getNumberOfArguments() {
    return 3;
  }
}
