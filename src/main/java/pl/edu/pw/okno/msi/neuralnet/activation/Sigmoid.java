package pl.edu.pw.okno.msi.neuralnet.activation;

import lombok.ToString;

@ToString
public class Sigmoid implements ActivationFunction {

  @Override
  public double getValue(double input) {
    return 1.0 / (1.0 + Math.exp(-input));
  }

  @Override
  public double getDerivative(double input) {
    double fx = getValue(input);
    return fx * (1.0 - fx);
  }
}
