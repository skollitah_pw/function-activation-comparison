package pl.edu.pw.okno.msi.utils;

import lombok.Getter;
import lombok.ToString;

@ToString
public class Pair<T, U> {

  @Getter private T value0;

  @Getter private U value1;

  private Pair(T value0, U value1) {
    this.value0 = value0;
    this.value1 = value1;
  }

  public static <T, U> Pair<T, U> with(T value0, U value1) {
    return new Pair<>(value0, value1);
  }
}
