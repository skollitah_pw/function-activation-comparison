package pl.edu.pw.okno.msi.function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import pl.edu.pw.okno.msi.utils.Pair;
import pl.edu.pw.okno.msi.utils.Preconditions;

public abstract class MArgumentFunction {

  public abstract Double getValue(List<Double> arguments);

  public abstract int getNumberOfArguments();

  public Pair<List<List<Double>>, List<List<Double>>> generateExamples(
      List<Pair<Double, Double>> argumentRanges, int numberOfExamples) {

    Preconditions.checkArgument(argumentRanges != null, "argumentRanges is null");
    Preconditions.checkArgument(
        argumentRanges.size() == getNumberOfArguments(),
        "range has different size than number of function arguments");
    Preconditions.checkArgument(numberOfExamples > 0, "numberOfExamples is less or equal 0");
    Preconditions.checkArgument(
        argumentRanges.stream()
            .allMatch(argumentRange -> argumentRange.getValue0() < argumentRange.getValue1()),
        "range from less or equal range to");

    List<List<Double>> inputValues = new ArrayList<>();
    List<List<Double>> outputValues = new ArrayList<>();

    IntStream.range(0, numberOfExamples)
        .forEach(
            exampleIndex -> {
              Pair<List<Double>, List<Double>> trainRecord = generateTrainRecord(argumentRanges);
              inputValues.add(trainRecord.getValue0());
              outputValues.add(trainRecord.getValue1());
            });

    return Pair.with(inputValues, outputValues);
  }

  private Pair<List<Double>, List<Double>> generateTrainRecord(
      List<Pair<Double, Double>> argumentRanges) {

    List<Double> inputValuesRecord = new ArrayList<>();

    argumentRanges.forEach(
        argumentRange ->
            inputValuesRecord.add(
                (Math.random()) * (argumentRange.getValue1() - argumentRange.getValue0())
                    + argumentRange.getValue0()));

    Double outputValue = getValue(inputValuesRecord);

    return Pair.with(inputValuesRecord, Collections.singletonList(outputValue));
  }
}
