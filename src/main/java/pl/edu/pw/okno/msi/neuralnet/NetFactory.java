package pl.edu.pw.okno.msi.neuralnet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;
import pl.edu.pw.okno.msi.neuralnet.neuron.Bias;
import pl.edu.pw.okno.msi.neuralnet.neuron.Hidden;
import pl.edu.pw.okno.msi.neuralnet.neuron.Input;
import pl.edu.pw.okno.msi.neuralnet.neuron.Output;
import pl.edu.pw.okno.msi.neuralnet.neuron.WeightedLink;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class NetFactory {

  public static Net create(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      ActivationFunction activationFunction,
      Double weightInitRange) {

    checkCommonCreateArguments(numberOfInputs, numberOfOutputs, hiddenLayersSizes);
    checkActivationFunctionArgument(activationFunction);
    checkWeightInitRangeArgument(weightInitRange);

    return create(
        numberOfInputs,
        numberOfOutputs,
        hiddenLayersSizes,
        activationFunction,
        activationFunction,
        weightInitRange,
        null);
  }

  public static Net create(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      ActivationFunction hiddenLayerActivationFunction,
      ActivationFunction outputLayerActivationFunction,
      double weightInitRange) {

    checkCommonCreateArguments(numberOfInputs, numberOfOutputs, hiddenLayersSizes);
    checkActivationFunctionArgument(hiddenLayerActivationFunction);
    checkActivationFunctionArgument(outputLayerActivationFunction);
    checkWeightInitRangeArgument(weightInitRange);

    return create(
        numberOfInputs,
        numberOfOutputs,
        hiddenLayersSizes,
        hiddenLayerActivationFunction,
        outputLayerActivationFunction,
        weightInitRange,
        null);
  }

  public static Net create(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      ActivationFunction hiddenLayerActivationFunction,
      ActivationFunction outputLayerActivationFunction,
      List<Double> initialWeights) {

    checkCommonCreateArguments(numberOfInputs, numberOfOutputs, hiddenLayersSizes);
    checkActivationFunctionArgument(hiddenLayerActivationFunction);
    checkActivationFunctionArgument(outputLayerActivationFunction);
    checkInitialWeightsArgument(numberOfInputs, numberOfOutputs, hiddenLayersSizes, initialWeights);

    return create(
        numberOfInputs,
        numberOfOutputs,
        hiddenLayersSizes,
        hiddenLayerActivationFunction,
        outputLayerActivationFunction,
        null,
        initialWeights);
  }

  private static void checkInitialWeightsArgument(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      List<Double> initialWeights) {
    Preconditions.checkArgument(
        new ExamplesGenerator()
                .getNumberOfWeightsForNetParameters(
                    numberOfInputs, numberOfOutputs, hiddenLayersSizes)
            == initialWeights.size(),
        "initialWeights has wrong size");
  }

  private static void checkCommonCreateArguments(
      int numberOfInputs, int numberOfOutputs, List<Integer> hiddenLayersSizes) {

    Preconditions.checkArgument(numberOfInputs > 0, "numberOfInputs is less or equal 0");
    Preconditions.checkArgument(numberOfOutputs > 0, "numberOfOutputs is less or equal 0");
    Preconditions.checkArgument(
        hiddenLayersSizes != null
            && hiddenLayersSizes.stream().allMatch(hiddenLayersSize -> hiddenLayersSize > 0),
        "hiddenLayersSizes is null or sizes less or equal 0");
  }

  private static void checkActivationFunctionArgument(ActivationFunction activationFunction) {
    Preconditions.checkArgument(activationFunction != null, "activationFunction is null");
  }

  private static void checkWeightInitRangeArgument(Double weightInitRange) {
    Preconditions.checkArgument(
        weightInitRange != null && weightInitRange > 0,
        "weightInitRange is null or less or equal 0");
  }

  private static Net create(
      int numberOfInputs,
      int numberOfOutputs,
      List<Integer> hiddenLayersSizes,
      ActivationFunction hiddenLayerActivationFunction,
      ActivationFunction outputLayerActivationFunction,
      Double weightInitRange,
      List<Double> initialWeights) {

    List<Input> inputs = crateInputs(numberOfInputs);
    List<Net.HiddenLayer> hiddenLayers =
        crateHiddenLayers(hiddenLayersSizes, hiddenLayerActivationFunction);
    List<Bias> hiddenBiases = createHiddenBiases(hiddenLayersSizes);
    List<Output> outputs =
        crateOutputs(numberOfOutputs, hiddenLayersSizes.size(), outputLayerActivationFunction);
    Bias outputBias = createOutputBias(numberOfOutputs, hiddenLayersSizes.size());

    List<WeightedLink> weightedLinks = new ArrayList<>();
    linkInputWithHidden(inputs, hiddenLayers, weightedLinks);
    linkHiddenLayers(hiddenLayers, weightedLinks);
    linkHiddenWithOutput(outputs, hiddenLayers, weightedLinks);
    linkHiddenBiasesWithHidden(hiddenBiases, hiddenLayers, weightedLinks);
    linkOutputBiasWithOutput(outputBias, outputs, weightedLinks);

    if (initialWeights != null) {
      if (initialWeights.size() != weightedLinks.size()) {
        throw new IllegalArgumentException(
            "initialWeights size different than  weightedLinks size");
      }

      setWeights(weightedLinks, initialWeights);
    } else {
      initRandomWeights(weightedLinks, weightInitRange);
    }

    return new Net(inputs, hiddenLayers, hiddenBiases, outputs, outputBias, weightedLinks);
  }

  static void setWeights(List<WeightedLink> weightedLinks, List<Double> initialWeights) {
    IntStream.range(0, weightedLinks.size())
        .forEach(index -> weightedLinks.get(index).setWeight(initialWeights.get(index)));
  }

  private static void initRandomWeights(List<WeightedLink> weightedLinks, double weightInitRange) {
    weightedLinks.forEach(
        weightedLink -> weightedLink.setWeight((Math.random() - 0.5) * weightInitRange));
  }

  private static List<Input> crateInputs(int numberOfInputs) {
    return IntStream.range(0, numberOfInputs)
        .mapToObj(index -> new Input(0, index))
        .collect(Collectors.toList());
  }

  private static List<Output> crateOutputs(
      int numberOfOutputs, int numberOfHiddenLayers, ActivationFunction activationFunction) {
    return IntStream.range(0, numberOfOutputs)
        .mapToObj(
            index -> {
              Output output = new Output(numberOfHiddenLayers + 1, index);
              output.setActivationFunction(activationFunction);
              return output;
            })
        .collect(Collectors.toList());
  }

  private static List<Net.HiddenLayer> crateHiddenLayers(
      List<Integer> hiddenLayersSizes, ActivationFunction activationFunction) {
    return IntStream.range(0, hiddenLayersSizes.size())
        .mapToObj(
            layerIndex ->
                new Net.HiddenLayer(
                    IntStream.range(0, hiddenLayersSizes.get(layerIndex))
                        .mapToObj(
                            index -> {
                              Hidden hidden = new Hidden(layerIndex + 1, index);
                              hidden.setActivationFunction(activationFunction);
                              return hidden;
                            })
                        .collect(Collectors.toList())))
        .collect(Collectors.toList());
  }

  private static List<Bias> createHiddenBiases(List<Integer> hiddenLayersSizes) {
    return IntStream.range(0, hiddenLayersSizes.size())
        .mapToObj(index -> new Bias(index, hiddenLayersSizes.get(index)))
        .collect(Collectors.toList());
  }

  private static Bias createOutputBias(int numberOfOutputs, int numberOfHiddenLayers) {
    return new Bias(numberOfHiddenLayers, numberOfOutputs);
  }

  private static void linkInputWithHidden(
      List<Input> inputs, List<Net.HiddenLayer> hiddenLayers, List<WeightedLink> weightedLinks) {

    inputs.forEach(
        input ->
            hiddenLayers
                .get(0)
                .getNeurons()
                .forEach(
                    neuron -> {
                      WeightedLink weightedLink =
                          new WeightedLink(
                              input.getLayerIndex(),
                              input.getIndex(),
                              neuron.getLayerIndex(),
                              neuron.getIndex());
                      weightedLink.setInput(input);
                      weightedLink.setOutput(neuron);
                      weightedLinks.add(weightedLink);

                      neuron.addInputLink(weightedLink);
                    }));
  }

  private static void linkHiddenLayers(
      List<Net.HiddenLayer> hiddenLayers, List<WeightedLink> weightedLinks) {

    IntStream.range(0, hiddenLayers.size() - 1)
        .forEach(
            layerIndex ->
                hiddenLayers
                    .get(layerIndex)
                    .getNeurons()
                    .forEach(
                        previous -> {
                          hiddenLayers
                              .get(layerIndex + 1)
                              .getNeurons()
                              .forEach(
                                  next -> {
                                    WeightedLink weightedLink =
                                        new WeightedLink(
                                            previous.getLayerIndex(),
                                            previous.getIndex(),
                                            next.getLayerIndex(),
                                            next.getIndex());
                                    weightedLink.setInput(previous);
                                    weightedLink.setOutput(next);
                                    weightedLinks.add(weightedLink);

                                    previous.addOutputLink(weightedLink);
                                    next.addInputLink(weightedLink);
                                  });
                        }));
  }

  private static void linkHiddenWithOutput(
      List<Output> outputs, List<Net.HiddenLayer> hiddenLayers, List<WeightedLink> weightedLinks) {

    outputs.forEach(
        output ->
            hiddenLayers
                .get(hiddenLayers.size() - 1)
                .getNeurons()
                .forEach(
                    neuron -> {
                      WeightedLink weightedLink =
                          new WeightedLink(
                              neuron.getLayerIndex(),
                              neuron.getIndex(),
                              output.getLayerIndex(),
                              output.getIndex());
                      weightedLink.setInput(neuron);
                      weightedLink.setOutput(output);
                      weightedLinks.add(weightedLink);

                      neuron.addOutputLink(weightedLink);
                      output.addInputLink(weightedLink);
                    }));
  }

  private static void linkHiddenBiasesWithHidden(
      List<Bias> hiddenBiases,
      List<Net.HiddenLayer> hiddenLayers,
      List<WeightedLink> weightedLinks) {

    IntStream.range(0, hiddenLayers.size())
        .forEach(
            layerIndex -> {
              hiddenLayers
                  .get(layerIndex)
                  .getNeurons()
                  .forEach(
                      neuron -> {
                        Bias bias = hiddenBiases.get(layerIndex);
                        WeightedLink weightedLink =
                            new WeightedLink(
                                bias.getLayerIndex(),
                                bias.getIndex(),
                                neuron.getLayerIndex(),
                                neuron.getIndex());
                        weightedLink.setInput(bias);
                        weightedLink.setOutput(neuron);
                        weightedLinks.add(weightedLink);

                        neuron.addInputLink(weightedLink);
                      });
            });
  }

  private static void linkOutputBiasWithOutput(
      Bias outputBias, List<Output> outputs, List<WeightedLink> weightedLinks) {

    outputs.forEach(
        output -> {
          WeightedLink weightedLink =
              new WeightedLink(
                  outputBias.getLayerIndex(),
                  outputBias.getIndex(),
                  output.getLayerIndex(),
                  output.getIndex());
          weightedLink.setInput(outputBias);
          weightedLink.setOutput(output);
          weightedLinks.add(weightedLink);

          output.addInputLink(weightedLink);
        });
  }
}
