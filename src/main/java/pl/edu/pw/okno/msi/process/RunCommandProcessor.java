package pl.edu.pw.okno.msi.process;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.argument.RunCommand;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;
import pl.edu.pw.okno.msi.neuralnet.Net;
import pl.edu.pw.okno.msi.neuralnet.NetFactory;
import pl.edu.pw.okno.msi.utils.Pair;

@Slf4j
@RequiredArgsConstructor
public class RunCommandProcessor {

  private final RunCommand runCommand;
  private final ExamplesGenerator examplesGenerator;
  private final CsvReaderWriter csvReaderWriter;

  public void process() {
    log.info("Run network for command: {}", runCommand);

    Path weightsInputPath = Paths.get(runCommand.getPathToWeights());

    List<List<Double>> weights = csvReaderWriter.read(weightsInputPath);

    log.info("Wights read from: {}", weightsInputPath.toAbsolutePath().toString());

    if (weights.size() != 1) {
      throw new IllegalArgumentException("Incorrect number of records with weights found");
    }

    Net net =
        NetFactory.create(
            runCommand.getNumberOfInputs(),
            runCommand.getNumberOfOutputs(),
            runCommand.getHiddenLayersSizes(),
            runCommand.getHiddenLayerActivationFunction(),
            runCommand.getOutputLayerActivationFunction(),
            weights.get(0));

    Path inputsPath = Paths.get(runCommand.getPathToInputs());

    List<List<Double>> inputsAndOutputs = csvReaderWriter.read(inputsPath);
    Pair<List<List<Double>>, List<List<Double>>> inputsAndOutputsPair =
        examplesGenerator.splitInputAndOutput(
            runCommand.getNumberOfInputs(), runCommand.getNumberOfOutputs(), inputsAndOutputs);

    log.info("Inputs read from: {}", inputsPath.toAbsolutePath().toString());

    List<Double> totalMeanErrorForOutputs =
        net.totalMeanErrorForOutputs(
            inputsAndOutputsPair.getValue0(), inputsAndOutputsPair.getValue1());

    log.info("Total average error for all examples {}", totalMeanErrorForOutputs);
  }
}
