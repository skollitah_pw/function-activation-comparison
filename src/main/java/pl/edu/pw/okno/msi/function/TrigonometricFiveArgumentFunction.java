package pl.edu.pw.okno.msi.function;

import java.util.List;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class TrigonometricFiveArgumentFunction extends MArgumentFunction {
  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);
    double z = arguments.get(2);
    double v = arguments.get(3);
    double w = arguments.get(4);

    return Math.sin(Math.pow(x, 3.0))
        - 0.3 * Math.cos(0.1 * Math.pow(y, 2.0))
        - 1.5 * Math.sin(3 * z)
        + 4.0 * Math.cos(0.5 * v)
        - 0.1 * Math.sin(w);
  }

  @Override
  public int getNumberOfArguments() {
    return 5;
  }
}
