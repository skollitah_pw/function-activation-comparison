package pl.edu.pw.okno.msi.neuralnet.neuron;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(exclude = "outputLinks")
public class Hidden extends Neuron {

  @Getter private final int layerIndex;
  @Getter private final int index;
  @Getter @Setter private List<WeightedLink> outputLinks;

  public Hidden(int layerIndex, int index) {
    this.layerIndex = layerIndex;
    this.index = index;
    computeIdentifier();
  }

  public double getErrorDerivative() {
    return outputLinks.stream()
        .mapToDouble(
            outputLink ->
                outputLink.getOutput().getErrorDerivative()
                    * outputLink.getOutput().getDerivative()
                    * outputLink.getWeight())
        .sum();
  }

  public void addOutputLink(WeightedLink outputLink) {
    if (outputLinks == null) {
      outputLinks = new ArrayList<>();
    }

    outputLinks.add(outputLink);
  }
}
