package pl.edu.pw.okno.msi.process;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.argument.GenerateExamplesCommand;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;

@Slf4j
@RequiredArgsConstructor
public class GenerateExamplesCommandProcessor {

  private final GenerateExamplesCommand generateExamplesCommand;
  private final ExamplesGenerator examplesGenerator;
  private final CsvReaderWriter csvReaderWriter;

  public void process() {
    log.info("Generate examples for command: " + generateExamplesCommand);

    List<List<Double>> generatedExamples =
        examplesGenerator.combineInputAndOutput(
            examplesGenerator.generateExamples(
                generateExamplesCommand.getInputRanges(),
                generateExamplesCommand.getMArgumentFunctions(),
                generateExamplesCommand.getNumberOfExamples()));

    Path outputPath = Paths.get(generateExamplesCommand.getPathToGeneratedExamples());

    csvReaderWriter.write(outputPath, generatedExamples);

    log.info(String.format("Examples generated to: %s", outputPath.toAbsolutePath().toString()));
  }
}
