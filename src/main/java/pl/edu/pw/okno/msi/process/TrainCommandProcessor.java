package pl.edu.pw.okno.msi.process;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.edu.pw.okno.msi.argument.TrainCommand;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;
import pl.edu.pw.okno.msi.neuralnet.Net;
import pl.edu.pw.okno.msi.neuralnet.NetFactory;
import pl.edu.pw.okno.msi.utils.Pair;

@Slf4j
@RequiredArgsConstructor
public class TrainCommandProcessor {

  private static final double DEFAULT_INITIAL_WEIGHTS_RANGE = 1.0;

  private final TrainCommand trainCommand;
  private final ExamplesGenerator examplesGenerator;
  private final CsvReaderWriter csvReaderWriter;

  public void process() {
    log.info("Run network for command: " + trainCommand);

    Net net = createNet();
    Pair<List<List<Double>>, List<List<Double>>> trainingExamples = getTrainingExamples();

    net.train(
        trainCommand.getNumOfIterations(),
        trainCommand.getExpectedTotalError(),
        trainCommand.getTrainingRate(),
        trainingExamples.getValue0(),
        trainingExamples.getValue1());

    storeFinalWeights(net.getWeights());
  }

  private Net createNet() {
    if (trainCommand.getPathToInitialWeights() != null) {
      Path weightsInputPath = Paths.get(trainCommand.getPathToInitialWeights());

      List<List<Double>> weights = csvReaderWriter.read(weightsInputPath);

      log.info("Wights read from: " + weightsInputPath.toAbsolutePath().toString());

      if (weights.size() != 1) {
        throw new IllegalArgumentException("Incorrect number of records with weights found");
      }

      return NetFactory.create(
          trainCommand.getNumberOfInputs(),
          trainCommand.getNumberOfOutputs(),
          trainCommand.getHiddenLayersSizes(),
          trainCommand.getHiddenLayerActivationFunction(),
          trainCommand.getOutputLayerActivationFunction(),
          weights.get(0));
    } else if (trainCommand.getInitialWeightsRange() != null) {
      return NetFactory.create(
          trainCommand.getNumberOfInputs(),
          trainCommand.getNumberOfOutputs(),
          trainCommand.getHiddenLayersSizes(),
          trainCommand.getHiddenLayerActivationFunction(),
          trainCommand.getOutputLayerActivationFunction(),
          trainCommand.getInitialWeightsRange());
    } else {
      return NetFactory.create(
          trainCommand.getNumberOfInputs(),
          trainCommand.getNumberOfOutputs(),
          trainCommand.getHiddenLayersSizes(),
          trainCommand.getHiddenLayerActivationFunction(),
          trainCommand.getOutputLayerActivationFunction(),
          DEFAULT_INITIAL_WEIGHTS_RANGE);
    }
  }

  private Pair<List<List<Double>>, List<List<Double>>> getTrainingExamples() {
    Path inputsPath = Paths.get(trainCommand.getPathToTrainingExamples());

    List<List<Double>> rawTrainingExamples = csvReaderWriter.read(inputsPath);

    log.info("Training examples read from: " + inputsPath.toAbsolutePath().toString());

    return examplesGenerator.splitInputAndOutput(
        trainCommand.getNumberOfInputs(), trainCommand.getNumberOfOutputs(), rawTrainingExamples);
  }

  private void storeFinalWeights(List<Double> finalWeights) {
    Path finalWeightsPath = Paths.get(trainCommand.getPathToOutputWeights());

    csvReaderWriter.write(finalWeightsPath, Collections.singletonList(finalWeights));

    log.info(String.format("Weights stored to: %s", finalWeightsPath.toAbsolutePath().toString()));
  }
}
