package pl.edu.pw.okno.msi.neuralnet.neuron;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public class Input implements ValueProvider {

  @Getter private final int layerIndex;
  @Getter private final int index;
  @Getter @Setter private double value;
}
