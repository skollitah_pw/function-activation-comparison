package pl.edu.pw.okno.msi.argument;

import static pl.edu.pw.okno.msi.argument.GenerateExamplesCommand.NAME;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pl.edu.pw.okno.msi.function.MArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricFiveArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricFourArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricOneArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricSevenArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricSixArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricThreeArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricTwoArgumentFunction;
import pl.edu.pw.okno.msi.utils.Pair;

@Parameters(commandNames = NAME, commandDescription = "Generate examples")
@Getter
@Setter
@ToString
public class GenerateExamplesCommand {

  public static final String NAME = "generate-examples";

  @Parameter(names = "-e", description = "Number of examples", required = true)
  private Integer numberOfExamples;

  @Parameter(
      names = "-f",
      description = "M argument function identifiers list separated by comma: F3,F5,F7",
      required = true,
      converter = FunctionConverter.class)
  private List<MArgumentFunction> mArgumentFunctions;

  @Parameter(
      names = "-r",
      description = "Network input arguments range list separated by comma: -1.0:1.0,-2.0:2.0",
      required = true,
      converter = InputRangesConverter.class)
  private List<Pair<Double, Double>> inputRanges;

  @Parameter(
      names = "--examples",
      description = "Path to generated training examples",
      required = true)
  private String pathToGeneratedExamples;

  private static class InputRangesConverter implements IStringConverter<Pair<Double, Double>> {

    @Override
    public Pair<Double, Double> convert(String inputRangeText) {
      String[] inputRangeFromAndTo = inputRangeText.split(":");

      if (inputRangeFromAndTo.length != 2) {
        throw new IllegalArgumentException("Incorrect input range: " + inputRangeText);
      }

      return Pair.with(
          Double.parseDouble(inputRangeFromAndTo[0]), Double.parseDouble(inputRangeFromAndTo[1]));
    }
  }

  private static class FunctionConverter implements IStringConverter<MArgumentFunction> {

    @Override
    public MArgumentFunction convert(String mArgumentFunctionCode) {

      MArgumentFunction mArgumentFunction;

      switch (mArgumentFunctionCode) {
        case "F1":
          mArgumentFunction = new TrigonometricOneArgumentFunction();
          break;
        case "F2":
          mArgumentFunction = new TrigonometricTwoArgumentFunction();
          break;
        case "F3":
          mArgumentFunction = new TrigonometricThreeArgumentFunction();
          break;
        case "F4":
          mArgumentFunction = new TrigonometricFourArgumentFunction();
          break;
        case "F5":
          mArgumentFunction = new TrigonometricFiveArgumentFunction();
          break;
        case "F6":
          mArgumentFunction = new TrigonometricSixArgumentFunction();
          break;
        case "F7":
          mArgumentFunction = new TrigonometricSevenArgumentFunction();
          break;
        default:
          throw new IllegalArgumentException("Incorrect function code: " + mArgumentFunctionCode);
      }

      return mArgumentFunction;
    }
  }
}
