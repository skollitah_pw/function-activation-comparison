package pl.edu.pw.okno.msi.function;

import java.util.List;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class TrigonometricThreeArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);
    double z = arguments.get(2);

    return Math.sin(x) + 0 * 2 * Math.cos(0.1 * y) - 1.5 * Math.sin(3 * z) + 3.0;
  }

  @Override
  public int getNumberOfArguments() {
    return 3;
  }
}
