package pl.edu.pw.okno.msi.neuralnet.activation;

import lombok.ToString;

@ToString
public class Linear implements ActivationFunction {

  @Override
  public double getValue(double input) {
    return input;
  }

  @Override
  public double getDerivative(double input) {
    return 1.0;
  }
}
