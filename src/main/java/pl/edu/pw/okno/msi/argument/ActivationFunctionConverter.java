package pl.edu.pw.okno.msi.argument;

import com.beust.jcommander.IStringConverter;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;
import pl.edu.pw.okno.msi.neuralnet.activation.Linear;
import pl.edu.pw.okno.msi.neuralnet.activation.ReLU;
import pl.edu.pw.okno.msi.neuralnet.activation.Sigmoid;

class ActivationFunctionConverter implements IStringConverter<ActivationFunction> {

  @Override
  public ActivationFunction convert(String s) {
    ActivationFunction activationFunction;

    switch (s) {
      case "R":
        activationFunction = new ReLU();
        break;
      case "L":
        activationFunction = new Linear();
        break;
      case "S":
        activationFunction = new Sigmoid();
        break;
      default:
        throw new IllegalArgumentException("Incorrect activation function code");
    }

    return activationFunction;
  }
}
