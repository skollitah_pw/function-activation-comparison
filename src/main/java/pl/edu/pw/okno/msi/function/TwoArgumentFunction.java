package pl.edu.pw.okno.msi.function;

import java.util.List;
import lombok.ToString;
import pl.edu.pw.okno.msi.utils.Preconditions;

@ToString
public class TwoArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);

    return 3.0 * Math.pow(x, 3.0) - Math.pow(y, 2);
  }

  @Override
  public int getNumberOfArguments() {
    return 2;
  }
}
