package pl.edu.pw.okno.msi.function;

import java.util.List;
import pl.edu.pw.okno.msi.utils.Preconditions;

public class TrigonometricSevenArgumentFunction extends MArgumentFunction {

  @Override
  public Double getValue(List<Double> arguments) {
    Preconditions.checkArgument(
        arguments.size() == getNumberOfArguments(),
        String.format("arguments has size different than %d", getNumberOfArguments()));

    double x = arguments.get(0);
    double y = arguments.get(1);
    double z = arguments.get(2);
    double v = arguments.get(3);
    double w = arguments.get(4);
    double m = arguments.get(5);
    double n = arguments.get(6);

    return -2.3 * Math.sin(Math.pow(x, 2.0))
        + 0.5 * Math.cos(3 * y)
        - 0.4 * Math.sin(3.0 * Math.pow(z, 3.0))
        + 4.0 * Math.cos(0.5 * v)
        - 0.4 * Math.sin(w)
        + 3.9 * Math.cos(Math.pow(m, 2.0))
        + Math.sin(n)
        + 2.0;
  }

  @Override
  public int getNumberOfArguments() {
    return 7;
  }
}
