package pl.edu.pw.okno.msi.process;

import pl.edu.pw.okno.msi.argument.GenerateExamplesCommand;
import pl.edu.pw.okno.msi.argument.GenerateWeightsCommand;
import pl.edu.pw.okno.msi.argument.RunCommand;
import pl.edu.pw.okno.msi.argument.TrainCommand;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;

public class CommandProcessor {

  private final ExamplesGenerator examplesGenerator = new ExamplesGenerator();
  private final CsvReaderWriter csvReaderWriter = new CsvReaderWriter();

  public void train(TrainCommand trainCommand) {
    new TrainCommandProcessor(trainCommand, examplesGenerator, csvReaderWriter).process();
  }

  public void run(RunCommand runCommand) {
    new RunCommandProcessor(runCommand, examplesGenerator, csvReaderWriter).process();
  }

  public void generateExamples(GenerateExamplesCommand generateExamplesCommand) {
    new GenerateExamplesCommandProcessor(
            generateExamplesCommand, examplesGenerator, csvReaderWriter)
        .process();
  }

  public void generateWeights(GenerateWeightsCommand generateWeightsCommand) {
    new GenerateWeightsCommandProcessor(generateWeightsCommand, examplesGenerator, csvReaderWriter)
        .process();
  }
}
