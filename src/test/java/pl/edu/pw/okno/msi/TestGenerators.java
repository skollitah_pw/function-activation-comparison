package pl.edu.pw.okno.msi;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;
import pl.edu.pw.okno.msi.function.FiveArgumentFunction;
import pl.edu.pw.okno.msi.function.SevenArgumentFunction;
import pl.edu.pw.okno.msi.function.ThreeArgumentFunction;
import pl.edu.pw.okno.msi.utils.Pair;

public class TestGenerators {
  private final ExamplesGenerator examplesGenerator = new ExamplesGenerator();
  private final CsvReaderWriter csvReaderWriter = new CsvReaderWriter();

  @Test
  @Disabled
  public void generateWeightsToFileForThreeFunctions() {
    List<Double> weights =
        examplesGenerator.generateWeights(7, 3, Collections.singletonList(30), 1.0);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/three_functions_weights.csv"),
        Collections.singletonList(weights));
  }

  @Test
  @Disabled
  public void generateExamplesToFileForThreeFunctions() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0)),
            Arrays.asList(
                new ThreeArgumentFunction(),
                new FiveArgumentFunction(),
                new SevenArgumentFunction()),
            100);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(Paths.get("./src/test/resources/three_functions_example.csv"), combined);
  }

  @Test
  @Disabled
  public void generateVerificationsToFileForThreeFunctions() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0)),
            Arrays.asList(
                new ThreeArgumentFunction(),
                new FiveArgumentFunction(),
                new SevenArgumentFunction()),
            50);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/three_functions_verification.csv"), combined);
  }

  @Test
  @Disabled
  public void generateWeightsToFileForThreeArgumentFunction() {
    List<Double> weights =
        examplesGenerator.generateWeights(3, 1, Collections.singletonList(20), 1.0);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/three_argument_weights.csv"),
        Collections.singletonList(weights));
  }

  @Test
  @Disabled
  public void generateExamplesToFileForThreeArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(Pair.with(-1.0, 1.0), Pair.with(-1.0, 1.0), Pair.with(-1.0, 1.0)),
            Collections.singletonList(new ThreeArgumentFunction()),
            100);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(Paths.get("./src/test/resources/three_argument_example.csv"), combined);
  }

  @Test
  @Disabled
  public void generateVerificationsToFileForThreeArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(Pair.with(-1.0, 1.0), Pair.with(-1.0, 1.0), Pair.with(-1.0, 1.0)),
            Collections.singletonList(new ThreeArgumentFunction()),
            20);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/three_argument_verification.csv"), combined);
  }

  @Test
  @Disabled
  public void generateWeightsToFileForFiveArgumentFunction() {
    List<Double> weights =
        examplesGenerator.generateWeights(5, 1, Collections.singletonList(25), 1.0);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/five_argument_weights.csv"),
        Collections.singletonList(weights));
  }

  @Test
  @Disabled
  public void generateExamplesToFileForFiveArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0)),
            Collections.singletonList(new FiveArgumentFunction()),
            100);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(Paths.get("./src/test/resources/five_argument_example.csv"), combined);
  }

  @Test
  @Disabled
  public void generateVerificationsToFileForFiveArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0),
                Pair.with(-10.0, 10.0)),
            Collections.singletonList(new FiveArgumentFunction()),
            5);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/five_argument_verification.csv"), combined);
  }

  @Test
  @Disabled
  public void generateWeightsToFileForSevenArgumentFunction() {
    List<Double> weights =
        examplesGenerator.generateWeights(7, 1, Collections.singletonList(30), 1.0);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/seven_argument_weights.csv"),
        Collections.singletonList(weights));
  }

  @Test
  @Disabled
  public void generateExamplesToFileForSevenArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0)),
            Collections.singletonList(new SevenArgumentFunction()),
            100);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(Paths.get("./src/test/resources/seven_argument_example.csv"), combined);
  }

  @Test
  @Disabled
  public void generateVerificationsToFileForSevenArgumentFunction() {
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(
            Arrays.asList(
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0),
                Pair.with(-1.0, 1.0)),
            Collections.singletonList(new SevenArgumentFunction()),
            10);

    List<List<Double>> combined = examplesGenerator.combineInputAndOutput(examples);

    csvReaderWriter.write(
        Paths.get("./src/test/resources/seven_argument_verification.csv"), combined);
  }
}
