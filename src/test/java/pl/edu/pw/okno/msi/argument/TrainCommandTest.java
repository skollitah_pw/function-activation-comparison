package pl.edu.pw.okno.msi.argument;

import static org.assertj.core.api.Assertions.assertThat;

import com.beust.jcommander.JCommander;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.edu.pw.okno.msi.neuralnet.activation.Linear;
import pl.edu.pw.okno.msi.neuralnet.activation.ReLU;

public class TrainCommandTest {

  @Test
  @DisplayName("should parse parameters for train command")
  public void shouldParseParametersForTrainCommand() {
    // given
    String[] args = {
      "train",
      "-i",
      "10",
      "-o",
      "5",
      "-h",
      "3,5",
      "-t",
      "23",
      "-e",
      "1.23",
      "-r",
      "1.0",
      "-a",
      "0.001",
      "--fh",
      "R",
      "--fo",
      "L",
      "--examples",
      "c:\\work\\training.csv",
      "--initial-weights",
      "c:\\work\\initial_weights.csv",
      "--output-weights",
      "c:\\work\\output_weights.csv"
    };

    // Build parser
    TrainCommand trainCommand = new TrainCommand();
    JCommander jc = JCommander.newBuilder().addCommand(trainCommand).build();

    // when
    jc.parse(args);

    // then
    assertThat(trainCommand.getNumberOfInputs()).isEqualTo(10);
    assertThat(trainCommand.getNumberOfOutputs()).isEqualTo(5);
    assertThat(trainCommand.getHiddenLayersSizes()).containsExactly(3, 5);
    assertThat(trainCommand.getNumOfIterations()).isEqualTo(23);
    assertThat(trainCommand.getInitialWeightsRange()).isEqualTo(1.0);
    assertThat(trainCommand.getTrainingRate()).isEqualTo(0.001);
    assertThat(trainCommand.getExpectedTotalError()).isEqualTo(1.23);
    assertThat(trainCommand.getHiddenLayerActivationFunction()).isInstanceOf(ReLU.class);
    assertThat(trainCommand.getOutputLayerActivationFunction()).isInstanceOf(Linear.class);
    assertThat(trainCommand.getPathToTrainingExamples()).isEqualTo("c:\\work\\training.csv");
    assertThat(trainCommand.getPathToInitialWeights()).isEqualTo("c:\\work\\initial_weights.csv");
    assertThat(trainCommand.getPathToOutputWeights()).isEqualTo("c:\\work\\output_weights.csv");
  }
}
