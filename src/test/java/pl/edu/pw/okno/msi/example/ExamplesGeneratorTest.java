package pl.edu.pw.okno.msi.example;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import pl.edu.pw.okno.msi.function.MArgumentFunction;
import pl.edu.pw.okno.msi.function.OneArgumentFunction;
import pl.edu.pw.okno.msi.function.ThreeArgumentFunction;
import pl.edu.pw.okno.msi.neuralnet.Net;
import pl.edu.pw.okno.msi.neuralnet.NetFactory;
import pl.edu.pw.okno.msi.neuralnet.activation.ReLU;
import pl.edu.pw.okno.msi.utils.Lists;
import pl.edu.pw.okno.msi.utils.Pair;

public class ExamplesGeneratorTest {

  private final ExamplesGenerator examplesGenerator = new ExamplesGenerator();

  @Test
  @DisplayName("should generate input and output examples for 1, 2, and 3 arguments functions")
  public void shouldGenerateExamplesForCoupleOfFunctions() {
    // given

    // Number of examples
    int numberOfExamples = 2;

    // M arguments Functions
    List<MArgumentFunction> mArgumentFunctions =
        Arrays.asList(new ThreeArgumentFunction(), new OneArgumentFunction());

    // Range of functions arguments
    List<Pair<Double, Double>> argumentRanges =
        Arrays.asList(Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0));

    // when
    Pair<List<List<Double>>, List<List<Double>>> examples =
        examplesGenerator.generateExamples(argumentRanges, mArgumentFunctions, numberOfExamples);

    // then
    IntStream.range(0, numberOfExamples)
        .forEach(
            index ->
                IntStream.range(0, mArgumentFunctions.size())
                    .forEach(
                        functionIndex -> {
                          MArgumentFunction mArgumentFunction =
                              mArgumentFunctions.get(functionIndex);
                          List<Double> inputArguments =
                              examples
                                  .getValue0()
                                  .get(index)
                                  .subList(0, mArgumentFunction.getNumberOfArguments());
                          double outputValue = mArgumentFunction.getValue(inputArguments);
                          double expectedOutputValue =
                              examples.getValue1().get(index).get(functionIndex);

                          assertThat(outputValue).isEqualTo(expectedOutputValue);
                        }));
  }

  @Test
  @DisplayName("should split list into inputs and outputs")
  public void shouldSplitInputAndOutput() {
    // given
    int numberOfInputs = 1;
    int numberOfOutputs = 2;
    List<List<Double>> combined =
        Arrays.asList(Arrays.asList(1.1, 1.2, 1.3), Arrays.asList(2.1, 2.2, 2.3));

    // when
    Pair<List<List<Double>>, List<List<Double>>> split =
        examplesGenerator.splitInputAndOutput(numberOfInputs, numberOfOutputs, combined);

    // then
    assertThat(split.getValue0()).hasSize(combined.size());
    assertThat(split.getValue1()).hasSize(combined.size());
    split.getValue0().forEach(record -> assertThat(record.size()).isEqualTo(numberOfInputs));
    split.getValue1().forEach(record -> assertThat(record.size()).isEqualTo(numberOfOutputs));
  }

  @Test
  @DisplayName("should combine lists of inputs and outputs into one list")
  public void shouldCombineListOfInputAndOutput() {
    // given
    List<List<Double>> inputRecords =
        Arrays.asList(Arrays.asList(1.1, 1.2), Arrays.asList(2.1, 2.2));
    List<List<Double>> outputRecords =
        Arrays.asList(Arrays.asList(1.3, 1.4), Arrays.asList(2.3, 2.4));

    // when
    List<List<Double>> combined =
        examplesGenerator.combineInputAndOutput(Pair.with(inputRecords, outputRecords));

    // then
    assertThat(combined).hasSize(inputRecords.size());
    IntStream.range(0, combined.size())
        .forEach(
            index ->
                assertThat(combined.get(index))
                    .containsExactlyElementsOf(
                        Lists.concat(inputRecords.get(index), outputRecords.get(index))));
  }

  @ParameterizedTest(
      name = "{index} => numberOfInputs={0}, numberOfOutputs={1}, hiddenLayersSizes={2}")
  @ArgumentsSource(GenerateWeightsArgumentsProvider.class)
  @DisplayName("should generate weights for network")
  public void shouldGenerateWeights(
      int numberOfInputs, int numberOfOutputs, List<Integer> hiddenLayersSizes) {
    // given
    Net net =
        NetFactory.create(numberOfInputs, numberOfOutputs, hiddenLayersSizes, new ReLU(), 1.0);

    // when
    List<Double> weights =
        examplesGenerator.generateWeights(numberOfInputs, numberOfOutputs, hiddenLayersSizes, 1.0);

    // then
    assertThat(weights).hasSameSizeAs(net.getWeights());
  }

  private static class GenerateWeightsArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(1, 1, Collections.singletonList(1)),
          Arguments.of(2, 3, Collections.singletonList(3)),
          Arguments.of(5, 10, Arrays.asList(8, 2, 6, 11)));
    }
  }
}
