package pl.edu.pw.okno.msi.neuralnet;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import pl.edu.pw.okno.msi.example.ExamplesGenerator;
import pl.edu.pw.okno.msi.file.CsvReaderWriter;
import pl.edu.pw.okno.msi.function.TrigonometricThreeArgumentFunction;
import pl.edu.pw.okno.msi.neuralnet.activation.ActivationFunction;
import pl.edu.pw.okno.msi.neuralnet.activation.Linear;
import pl.edu.pw.okno.msi.neuralnet.activation.ReLU;
import pl.edu.pw.okno.msi.neuralnet.activation.Sigmoid;
import pl.edu.pw.okno.msi.utils.Pair;

public class NetTest {

  private final ExamplesGenerator examplesGenerator = new ExamplesGenerator();
  private final CsvReaderWriter csvReaderWriter = new CsvReaderWriter();

  @Test
  @DisplayName("net should produce valid output when propagate forward")
  public void testNetworkForwardPropagation() {
    // given

    // Create neural network with 2 inputs, 2 outputs, single hidden layer with 2 neurons and
    // training rate 0.5. All neurons have sigmoid activation function
    Net net = createTestNetwork();

    // when
    List<Double> outputValues = net.forwardPropagate(Arrays.asList(0.05, 0.1));

    // then
    assertThat(outputValues).hasSize(2);
    assertThat(outputValues.get(0)).isCloseTo(0.75136506, Offset.offset(1e-8));
    assertThat(outputValues.get(1)).isCloseTo(0.77292846, Offset.offset(1e-8));
  }

  @Test
  @DisplayName("net should back propagate error and update weights")
  public void testNetworkBackPropagation() {
    // given

    // Create neural network with 2 inputs, 2 outputs, single hidden layer with 2 neurons and
    // training rate 0.5. All neurons have sigmoid activation function
    Net net = createTestNetwork();

    // when
    net.backPropagateError(Arrays.asList(0.05, 0.1), Arrays.asList(0.01, 0.99), 0.5);

    // then
    assertThat(net.findLink(0, 0, 1, 0).getWeight()).isCloseTo(0.14978071, Offset.offset(1e-8));
    assertThat(net.findLink(0, 1, 1, 0).getWeight()).isCloseTo(0.19956143, Offset.offset(1e-8));
    assertThat(net.findLink(0, 2, 1, 0).getWeight()).isCloseTo(0.34561432, Offset.offset(1e-8));
    assertThat(net.findLink(0, 0, 1, 1).getWeight()).isCloseTo(0.24975114, Offset.offset(1e-8));
    assertThat(net.findLink(0, 1, 1, 1).getWeight()).isCloseTo(0.29950229, Offset.offset(1e-8));
    assertThat(net.findLink(0, 2, 1, 1).getWeight()).isCloseTo(0.34502287, Offset.offset(1e-8));
    assertThat(net.findLink(0, 0, 1, 1).getWeight()).isCloseTo(0.24975114, Offset.offset(1e-8));
    assertThat(net.findLink(1, 0, 2, 0).getWeight()).isCloseTo(0.35891648, Offset.offset(1e-8));
    assertThat(net.findLink(1, 1, 2, 0).getWeight()).isCloseTo(0.40866618, Offset.offset(1e-8));
    assertThat(net.findLink(1, 2, 2, 0).getWeight()).isCloseTo(0.53075071, Offset.offset(1e-8));
    assertThat(net.findLink(1, 0, 2, 1).getWeight()).isCloseTo(0.51130127, Offset.offset(1e-8));
    assertThat(net.findLink(1, 1, 2, 1).getWeight()).isCloseTo(0.56137012, Offset.offset(1e-8));
    assertThat(net.findLink(1, 2, 2, 1).getWeight()).isCloseTo(0.61904911, Offset.offset(1e-8));
  }

  private Net createTestNetwork() {
    Net net = NetFactory.create(2, 2, Collections.singletonList(2), new Sigmoid(), 1.0);

    // Links from input to hidden layer
    net.findLink(0, 0, 1, 0).setWeight(0.15);
    net.findLink(0, 0, 1, 1).setWeight(0.25);
    net.findLink(0, 1, 1, 0).setWeight(0.2);
    net.findLink(0, 1, 1, 1).setWeight(0.3);
    net.findLink(0, 2, 1, 0).setWeight(0.35);
    net.findLink(0, 2, 1, 1).setWeight(0.35);

    // Links from hidden layer to output
    net.findLink(1, 0, 2, 0).setWeight(0.4);
    net.findLink(1, 0, 2, 1).setWeight(0.5);
    net.findLink(1, 1, 2, 0).setWeight(0.45);
    net.findLink(1, 1, 2, 1).setWeight(0.55);
    net.findLink(1, 2, 2, 0).setWeight(0.6);
    net.findLink(1, 2, 2, 1).setWeight(0.6);

    return net;
  }

  @Test
  @DisplayName(
      "net should be trained to classify two separate sets of data using sigmoid activation function")
  public void shouldTrainNetworkToClassify() {
    // given
    Net net = NetFactory.create(2, 2, Arrays.asList(4, 4), new Sigmoid(), 1.0);

    // when
    net.train(200, 0.05, 0.1, sigmoidTrainInputs(), sigmoidTrainOutputs());

    // then
    assertThat(net.totalError(Arrays.asList(-3.0, -3.0), Arrays.asList(1.0, 0.0))).isLessThan(0.1);
    assertThat(net.totalError(Arrays.asList(3.0, 3.0), Arrays.asList(0.0, 1.0))).isLessThan(0.1);
  }

  private List<List<Double>> sigmoidTrainInputs() {
    return Arrays.asList(
        Arrays.asList(-3.0, -2.0),
        Arrays.asList(-4.0, -3.0),
        Arrays.asList(-3.0, -4.0),
        Arrays.asList(-2.0, -3.0),
        Arrays.asList(3.0, 2.0),
        Arrays.asList(4.0, 3.0),
        Arrays.asList(3.0, 4.0),
        Arrays.asList(2.0, 3.0));
  }

  private List<List<Double>> sigmoidTrainOutputs() {
    return Arrays.asList(
        Arrays.asList(1.0, 0.0),
        Arrays.asList(1.0, 0.0),
        Arrays.asList(1.0, 0.0),
        Arrays.asList(1.0, 0.0),
        Arrays.asList(0.0, 1.0),
        Arrays.asList(0.0, 1.0),
        Arrays.asList(0.0, 1.0),
        Arrays.asList(0.0, 1.0));
  }

  @ParameterizedTest(
      name =
          "{index} => numberOfNetInputs={0}, numberOfHiddenNeurons={1}, activation={2}, trainingRate={3}, initialWeightsCsv={4}, examplesCsv={5}, verificationsCsv={6}")
  @ArgumentsSource(FunctionApproximationArgumentsProvider.class)
  @DisplayName("net should be trained to approximate non linear function")
  public void shouldTrainNetworkToApproximate(
      int numberOfNetInputs,
      int numberOfHiddenNeurons,
      ActivationFunction hiddenLayerActivationFunction,
      double trainingRate,
      String initialWeightsCsv,
      String examplesCsv,
      String verificationsCsv) {

    // given

    // Read weights from CSV file
    List<List<Double>> initialWeights =
        csvReaderWriter.read(csvReaderWriter.getFilePathFromClassPath(initialWeightsCsv));

    // Read training examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> trainSet =
        examplesGenerator.splitInputAndOutput(
            numberOfNetInputs,
            1,
            csvReaderWriter.read(csvReaderWriter.getFilePathFromClassPath(examplesCsv)));

    // Read Verification examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> verificationExamples =
        examplesGenerator.splitInputAndOutput(
            numberOfNetInputs,
            1,
            csvReaderWriter.read(csvReaderWriter.getFilePathFromClassPath(verificationsCsv)));

    // Test network
    Net net =
        NetFactory.create(
            numberOfNetInputs,
            1,
            Collections.singletonList(numberOfHiddenNeurons),
            hiddenLayerActivationFunction,
            new Linear(),
            initialWeights.get(0));

    // Total error before training
    Double before =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // when
    net.train(30, 1, trainingRate, trainSet.getValue0(), trainSet.getValue1());

    // Total error after training
    Double after =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // then
    assertThat(after).isLessThan(before);
  }

  static class FunctionApproximationArgumentsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(
              3,
              20,
              new Linear(),
              0.00001,
              "three_argument_weights.csv",
              "three_argument_example.csv",
              "three_argument_verification.csv"),
          Arguments.of(
              5,
              25,
              new Sigmoid(),
              0.01,
              "five_argument_weights.csv",
              "five_argument_example.csv",
              "five_argument_verification.csv"),
          Arguments.of(
              7,
              30,
              new ReLU(),
              0.0001,
              "seven_argument_weights.csv",
              "seven_argument_example.csv",
              "seven_argument_verification.csv"));
    }
  }

  @Test
  @DisplayName("net should be trained to approximate 3 non linear function at once")
  public void shouldTrainNetworkToApproximateThreeFunctionAtOnce() {

    // given

    // Read weights from CSV file
    List<List<Double>> initialWeights =
        csvReaderWriter.read(
            csvReaderWriter.getFilePathFromClassPath("three_functions_weights.csv"));

    // Read training examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> trainSet =
        examplesGenerator.splitInputAndOutput(
            7,
            3,
            csvReaderWriter.read(
                csvReaderWriter.getFilePathFromClassPath("three_functions_example.csv")));

    // Read Verification examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> verificationExamples =
        examplesGenerator.splitInputAndOutput(
            7,
            3,
            csvReaderWriter.read(
                csvReaderWriter.getFilePathFromClassPath("three_functions_verification.csv")));

    // Create test network
    Net net =
        NetFactory.create(
            7,
            3,
            Collections.singletonList(30),
            new Sigmoid(),
            new Linear(),
            initialWeights.get(0));

    // Total error before training
    Double before =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // when
    net.train(150, 1, 0.00001, trainSet.getValue0(), trainSet.getValue1());

    // Total error after training
    Double after =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // then
    assertThat(after).isLessThan(before);
  }

  @Test
  @DisplayName("net should be trained to approximate trigonometric function")
  public void shouldTrainNetworkToApproximateTrigonometricFunction() {
    // given

    // Read training examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> trainSet =
        examplesGenerator.generateExamples(
            Arrays.asList(Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0)),
            Collections.singletonList(new TrigonometricThreeArgumentFunction()),
            300);

    // Read Verification examples from CSV file
    Pair<List<List<Double>>, List<List<Double>>> verificationExamples =
        examplesGenerator.generateExamples(
            Arrays.asList(Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0), Pair.with(-10.0, 10.0)),
            Collections.singletonList(new TrigonometricThreeArgumentFunction()),
            10);

    // Test network
    Net net = NetFactory.create(3, 1, Collections.singletonList(15), new ReLU(), new Linear(), 1.0);

    // Total error before training
    Double before =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // when
    net.train(100, 1, 0.000001, trainSet.getValue0(), trainSet.getValue1());

    // Total error after training
    Double after =
        IntStream.range(0, verificationExamples.getValue0().size())
            .mapToDouble(
                exampleIndex ->
                    net.totalError(
                        verificationExamples.getValue0().get(exampleIndex),
                        verificationExamples.getValue1().get(exampleIndex)))
            .sum();

    // then
    assertThat(after).isLessThan(before);
  }
}
