package pl.edu.pw.okno.msi.file;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

public class CsvReaderWriterTest {

  private final CsvReaderWriter csvReaderWriter = new CsvReaderWriter();

  @Test
  public void shouldReadFileFromClassPath() {
    // when
    Path path = csvReaderWriter.getFilePathFromClassPath("values.csv");

    // then
    assertThat(path).isNotNull();
  }

  @Test
  public void shouldWriteAndReadCsvFile() throws IOException {
    // given
    Path path = File.createTempFile("values.csv", null).toPath();
    List<List<Double>> expectedRecords =
        Arrays.asList(Arrays.asList(1.1, 1.2, 1.3), Arrays.asList(2.1, 2.2, 2.3));

    // when
    csvReaderWriter.write(path, expectedRecords);
    List<List<Double>> storedRecords = csvReaderWriter.read(path);

    // then
    assertThat(storedRecords).isNotNull();
    assertThat(storedRecords).hasSize(expectedRecords.size());
    IntStream.range(0, storedRecords.size())
        .forEach(
            index -> {
              List<Double> storedRecord = storedRecords.get(index);
              assertThat(storedRecord).containsExactlyElementsOf(expectedRecords.get(index));
            });
  }
}
