package pl.edu.pw.okno.msi.argument;

import static org.assertj.core.api.Assertions.assertThat;

import com.beust.jcommander.JCommander;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GenerateWeightsCommandTest {

  @Test
  @DisplayName("should parse parameters for generate wights command")
  public void shouldParseParametersForGenerateWeightsCommand() {
    // given
    String[] args = {
      "generate-weights",
      "-i",
      "10",
      "-o",
      "5",
      "-h",
      "3,5",
      "-r",
      "6.3",
      "--weights",
      "c:\\work\\weights.csv"
    };

    // Build parser
    GenerateWeightsCommand generateWeightsCommand = new GenerateWeightsCommand();
    JCommander jc = JCommander.newBuilder().addCommand(generateWeightsCommand).build();

    // when
    jc.parse(args);

    // then
    assertThat(generateWeightsCommand.getNumberOfInputs()).isEqualTo(10);
    assertThat(generateWeightsCommand.getNumberOfOutputs()).isEqualTo(5);
    assertThat(generateWeightsCommand.getHiddenLayersSizes()).containsExactly(3, 5);
    assertThat(generateWeightsCommand.getWeightInitRange()).isEqualTo(6.3);
    assertThat(generateWeightsCommand.getPathToGeneratedWeights())
        .isEqualTo("c:\\work\\weights.csv");
  }
}
