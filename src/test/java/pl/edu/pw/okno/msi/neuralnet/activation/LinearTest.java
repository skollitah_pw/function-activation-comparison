package pl.edu.pw.okno.msi.neuralnet.activation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

public class LinearTest {

  private Linear linear = new Linear();

  @ParameterizedTest(name = "{index} => input={0}, expectedOutput={1}")
  @ArgumentsSource(LinearArgumentProvider.class)
  @DisplayName("should apply linear function to input")
  public void shouldApplyLinearFunctionToInput(double input, double expectedOutput) {
    // given
    // when
    double output = linear.getValue(input);
    // then
    assertThat(output).isEqualTo(expectedOutput);
  }

  static class LinearArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(-100.0, -100.0),
          Arguments.of(-1.0, -1.0),
          Arguments.of(0.0, 0.0),
          Arguments.of(1.0, 1.0),
          Arguments.of(100.0, 100.0));
    }
  }

  @ParameterizedTest(name = "{index} => input={0}, expectedDerivative=1.0")
  @ValueSource(doubles = {-10, -1.0, 0.0, 1.0, 10.0})
  @DisplayName("should always return 1.0 as derivative")
  public void shouldCalculateDerivative(double input) {
    // given
    double expectedDerivative = 1.0;
    // when
    double derivative = linear.getDerivative(input);
    // then
    assertThat(derivative).isEqualTo(expectedDerivative);
  }
}
