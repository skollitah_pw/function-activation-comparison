package pl.edu.pw.okno.msi.argument;

import static org.assertj.core.api.Assertions.assertThat;

import com.beust.jcommander.JCommander;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.edu.pw.okno.msi.neuralnet.activation.Linear;
import pl.edu.pw.okno.msi.neuralnet.activation.ReLU;

public class RunCommandTest {

  @Test
  @DisplayName("should parse parameters for run command")
  public void shouldParseParametersForRunCommand() {
    // given
    String[] args = {
      "run",
      "-i",
      "10",
      "-o",
      "5",
      "-h",
      "3,5",
      "--fh",
      "R",
      "--fo",
      "L",
      "--inputs",
      "c:\\work\\inputs.csv",
      "--weights",
      "c:\\work\\weights.csv"
    };

    // Build parser
    RunCommand runCommand = new RunCommand();
    JCommander jc = JCommander.newBuilder().addCommand(runCommand).build();

    // when
    jc.parse(args);

    // then
    assertThat(runCommand.getNumberOfInputs()).isEqualTo(10);
    assertThat(runCommand.getNumberOfOutputs()).isEqualTo(5);
    assertThat(runCommand.getHiddenLayersSizes()).containsExactly(3, 5);
    assertThat(runCommand.getHiddenLayerActivationFunction()).isInstanceOf(ReLU.class);
    assertThat(runCommand.getOutputLayerActivationFunction()).isInstanceOf(Linear.class);
    assertThat(runCommand.getPathToInputs()).isEqualTo("c:\\work\\inputs.csv");
    assertThat(runCommand.getPathToWeights()).isEqualTo("c:\\work\\weights.csv");
  }
}
