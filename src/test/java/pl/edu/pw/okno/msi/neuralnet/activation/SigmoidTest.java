package pl.edu.pw.okno.msi.neuralnet.activation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class SigmoidTest {

  private Sigmoid sigmoid = new Sigmoid();

  @ParameterizedTest(name = "{index} => input={0}, expectedOutput={1}")
  @ArgumentsSource(SigmoidArgumentProvider.class)
  @DisplayName("should apply Sigmoid function to input")
  public void shouldApplySigmoidFunctionToInput(double input, double expectedOutput) {
    // given
    // when
    double output = sigmoid.getValue(input);
    // then
    assertThat(output).isCloseTo(expectedOutput, Offset.offset(1e-5));
  }

  static class SigmoidArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(-100.0, 0.0),
          Arguments.of(-2.0, 0.11920),
          Arguments.of(-1.0, 0.26894),
          Arguments.of(0.0, 0.5),
          Arguments.of(1.0, 0.73105),
          Arguments.of(2.0, 0.88079),
          Arguments.of(100.0, 1.0));
    }
  }

  @ParameterizedTest(name = "{index} => input={0}, expectedDerivative={1}")
  @ArgumentsSource(SigmoidDerivativeArgumentProvider.class)
  @DisplayName("should return derivative for input")
  public void shouldCalculateDerivative(double input, double expectedDerivative) {
    // given
    // when
    double derivative = sigmoid.getDerivative(input);
    // then
    assertThat(derivative).isCloseTo(expectedDerivative, Offset.offset(1e-5));
  }

  static class SigmoidDerivativeArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(-100.0, 0.0),
          Arguments.of(-2.0, 0.10499),
          Arguments.of(-1.0, 0.19661),
          Arguments.of(0.0, 0.25),
          Arguments.of(1.0, 0.19661),
          Arguments.of(2.0, 0.10499),
          Arguments.of(100.0, 0.0));
    }
  }
}
