package pl.edu.pw.okno.msi.neuralnet.activation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

public class ReLUTest {

  private ReLU reLU = new ReLU();

  @ParameterizedTest(name = "{index} => input={0}, expectedOutput={1}")
  @ArgumentsSource(ReLUArgumentProvider.class)
  @DisplayName("should apply ReLU function to input")
  public void shouldApplyReLUFunctionToInput(double input, double expectedOutput) {
    // given
    // when
    double output = reLU.getValue(input);
    // then
    assertThat(output).isEqualTo(expectedOutput);
  }

  static class ReLUArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(-100.0, 0.0),
          Arguments.of(-1.0, 0.0),
          Arguments.of(0.0, 0.0),
          Arguments.of(1.0, 1.0),
          Arguments.of(100.0, 100.0));
    }
  }

  @ParameterizedTest(name = "{index} => input={0}, expectedDerivative={1}")
  @ArgumentsSource(ReLUDerivativeArgumentProvider.class)
  @DisplayName("should return derivative for input")
  public void shouldCalculateDerivative(double input, double expectedDerivative) {
    // given
    // when
    double derivative = reLU.getDerivative(input);
    // then
    assertThat(derivative).isEqualTo(expectedDerivative);
  }

  static class ReLUDerivativeArgumentProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
      return Stream.of(
          Arguments.of(-100.0, 0.0),
          Arguments.of(-1.0, 0.0),
          Arguments.of(0.0, 0.0),
          Arguments.of(1.0, 1.0),
          Arguments.of(100.0, 1.0),
          Arguments.of(1000.0, 1.0));
    }
  }
}
