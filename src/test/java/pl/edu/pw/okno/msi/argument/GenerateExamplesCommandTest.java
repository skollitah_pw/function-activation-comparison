package pl.edu.pw.okno.msi.argument;

import static org.assertj.core.api.Assertions.assertThat;

import com.beust.jcommander.JCommander;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.edu.pw.okno.msi.function.TrigonometricSevenArgumentFunction;
import pl.edu.pw.okno.msi.function.TrigonometricThreeArgumentFunction;

public class GenerateExamplesCommandTest {

  @Test
  @DisplayName("should parse parameters for generate examples command")
  public void shouldParseParametersForGenerateExamplesCommand() {
    // given
    String[] args = {
      "generate-examples",
      "-e",
      "100",
      "-r",
      "-1.2:3.5,-2.3:3.0",
      "-f",
      "F3,F7",
      "--examples",
      "c:\\work\\training.csv"
    };

    // Build parser
    GenerateExamplesCommand generateExamplesCommand = new GenerateExamplesCommand();
    JCommander jc = JCommander.newBuilder().addCommand(generateExamplesCommand).build();

    // when
    jc.parse(args);

    // then
    assertThat(generateExamplesCommand.getNumberOfExamples()).isEqualTo(100);
    assertThat(generateExamplesCommand.getPathToGeneratedExamples())
        .isEqualTo("c:\\work\\training.csv");
    assertThat(generateExamplesCommand.getInputRanges()).hasSize(2);
    assertThat(generateExamplesCommand.getInputRanges().get(0).getValue0()).isEqualTo(-1.2);
    assertThat(generateExamplesCommand.getInputRanges().get(0).getValue1()).isEqualTo(3.5);
    assertThat(generateExamplesCommand.getInputRanges().get(1).getValue0()).isEqualTo(-2.3);
    assertThat(generateExamplesCommand.getInputRanges().get(1).getValue1()).isEqualTo(3.0);
    assertThat(generateExamplesCommand.getMArgumentFunctions().get(0))
        .isInstanceOf(TrigonometricThreeArgumentFunction.class);
    assertThat(generateExamplesCommand.getMArgumentFunctions().get(1))
        .isInstanceOf(TrigonometricSevenArgumentFunction.class);
  }
}
